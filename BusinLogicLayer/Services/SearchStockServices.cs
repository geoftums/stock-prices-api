﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innova.StockPricesImporter.Data;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Runtime.InteropServices;
using Yogesh.ExcelXml;
using Yogesh.Extensions;
using BusinLogicLayer.BusinessModels;

namespace BusinLogicLayer.Services
{
    public  class SearchStockServices
    {
        #region Properties
        InnovaEntities_DB db = new InnovaEntities_DB();
        #endregion

        #region Methods
        public StockViewModel SearchStock(string name)
        {
            var stock = new StockViewModel();

            var search_stock = db.StockPrices.Where(t => t.Stock.Name == name).FirstOrDefault();
            try
            {
                stock.High = search_stock.High;
                stock.Low = search_stock.Low;
                stock.marketPrice = search_stock.MarketPrice;
                stock.stockName = search_stock.Stock.Name;
                stock.sharesTraded = search_stock.SharesTraded;
            }
            catch (Exception e)
            {
            }
            return stock;
        }
        #endregion
    }
}
