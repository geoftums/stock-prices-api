﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Innova.StockPricesImporter.Data;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using System.Runtime.InteropServices;
using Yogesh.ExcelXml;
using Yogesh.Extensions;
using BusinLogicLayer.BusinessModels;

namespace BusinLogicLayer.Services
{
    /// <summary>
    /// Class to Read Excel-xml files ,store them to the db and perform a search
    /// </summary>
    public class StockServices
    {
        #region Properties
        InnovaEntities_DB db = new InnovaEntities_DB();
        #endregion

        #region Methods
        public void SaveStockFile()
        {
            Worksheet uploaded_sheet;
            //stores the ISIN Code
            string isnCode = null;

            try
            {
                //subject to change based on the mapped path on the server
                //An example of an Excel file that contains the stock data
                //Price List -05-October-2016 was used as an example
                string file_Name = "C:\\Test\\price5.xml";
                Stream stockfile = File.OpenRead(file_Name); 
                var stockWorkBook = ExcelXmlWorkbook.Import(stockfile);

                uploaded_sheet = stockWorkBook.GetSheetByName("Sheet1");
                var test = db.Stocks.ToList();
                //Our tables
                StockPrice stockPriceTable = new StockPrice();
                Stock _stocks = new Stock();


                var stockDate = uploaded_sheet[9, 1]?.Value.ToString();

                #region For Statement
                //Our For statement to store data
                for (int i = 4; i < uploaded_sheet.RowCount; i++)
                {

                    bool hasvalue = false;
                    //checks if the worksheet contains any values
                    for (int u = 0; u < uploaded_sheet.ColumnCount; u++)
                    {
                        if (uploaded_sheet[i][u].Value != null)
                        {
                            hasvalue = true;
                            break;
                        }
                    }
                    if (!hasvalue) continue;

                    //check to see if there is any data 
                    if (uploaded_sheet[i][3].Value != null)
                    {
                        try
                        {
                            isnCode = uploaded_sheet[i][3]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    if (uploaded_sheet[i][5].Value != null)
                    {
                        try
                        {
                            stockPriceTable.High = Decimal.Parse(uploaded_sheet[i][5]?.Value.ToString());
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][6].Value != null)
                    {
                        try
                        {
                            stockPriceTable.Low = Decimal.Parse(uploaded_sheet[i][6]?.Value.ToString());
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][7].Value != null)
                    {
                        try
                        {
                            stockPriceTable.MarketPrice = Decimal.Parse(uploaded_sheet[i][7]?.Value.ToString());
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][8].Value != null)
                    {
                        try
                        {
                            //use Column I incase H(VWAP) is empty
                            if (uploaded_sheet[i][7].Value == null)
                            {
                                stockPriceTable.MarketPrice = Decimal.Parse(uploaded_sheet[i][8]?.Value.ToString());
                            }

                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][9].Value != null)
                    {
                        try
                        {
                            stockPriceTable.SharesTraded = int.Parse(uploaded_sheet[i][7]?.Value.ToString());
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    //search for the ISIN code in the stocks table to get the stockid
                    //so as to save it in the stock prices table
                    var searchedStock = db.Stocks.Where(t => t.ISIN == isnCode).FirstOrDefault();
                    stockPriceTable.stock_ID = searchedStock.stock_id;
                    db.StockPrices.Add(stockPriceTable);
                    db.SaveChanges();

                }
                #endregion


            }
            catch (Exception e)
            {

                
            }
        }

        public List<StockViewModel> GetStocks()
        {
            var stockPrice = new List<StockViewModel>();

            foreach (var item in db.StockPrices.ToList())
            {
                stockPrice.Add(
                    new StockViewModel
                    {
                        High = item.High,
                        Low=item.Low,
                        marketPrice=item.MarketPrice,
                        sharesTraded=item.SharesTraded,
                        stockName=item.Stock.Name
                    });
            }

            return stockPrice;
        }
        #endregion
    }
}
