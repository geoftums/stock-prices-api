﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinLogicLayer.BusinessModels
{
    /// <summary>
    /// A class that holds the data to be displayed on the grid
    /// </summary>
    public class StockViewModel
    {
        public string stockName { get; set; }
        public DateTime? marketPriceDate { get; set; }
        public Decimal? marketPrice { get; set; }
        public Decimal? High { get; set; }
        public Decimal? Low { get; set; }
        public int? sharesTraded { get; set; }
    }
}
