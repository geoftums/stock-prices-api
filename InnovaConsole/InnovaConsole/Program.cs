﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.IO;
using Yogesh.ExcelXml;
using Yogesh.Extensions;

namespace InnovaConsole
{
    /// <summary>
    /// Console app to Store Related Data in the Db.
    /// I stored all the files in my drive C:\\Test..under Test directory
    /// and read them from there
    /// </summary>
   public class Program
    {
        InnovaEntities db = new InnovaEntities();
        public void SaveStockFile()
        {
            Worksheet uploaded_sheet;
            int countRecs = 0;
            try
            {
                string file_Name = "C:\\Test\\myStocks.xml";
                Stream stockfile = File.OpenRead(file_Name);
                var stockWorkBook = ExcelXmlWorkbook.Import(stockfile);

                uploaded_sheet = stockWorkBook.GetSheetByName("Stocks");
                var stockID = uploaded_sheet[0, 1].Value;

                Stock _stocks = new Stock();

                #region For Statement
                //Our For statement to store data
                for (int i = 1; i < uploaded_sheet.RowCount; i++)
                {
                    
                    bool hasvalue = false;
                    //checks if the worksheet contains any values
                    for (int u = 0; u < uploaded_sheet.ColumnCount; u++)
                    {
                        if (uploaded_sheet[i][u].Value != null)
                        {
                            hasvalue = true;
                            break;
                        }
                    }
                    if (!hasvalue) continue;

                    //check to see if there is any data 
                    if (uploaded_sheet[i][0].Value != null)
                    {
                        try
                        {
                            ;
                            string test = uploaded_sheet[i][0]?.Value.ToString();
                           
                            _stocks.StockID= test;
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    if (uploaded_sheet[i][1].Value != null)
                    {
                        try
                        {
                            _stocks.Stock_TypeID = uploaded_sheet[i][1]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][2].Value != null)
                    {
                        try
                        {
                            _stocks.Stock_ExchangeID = uploaded_sheet[i][2]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }


                    if (uploaded_sheet[i][3].Value != null)
                    {
                        try
                        {
                            _stocks.Name = uploaded_sheet[i][3]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][4].Value != null)
                    {
                        try
                        {
                            _stocks.ISIN = uploaded_sheet[i][4]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][5].Value != null)
                    {
                        try
                        {
                            _stocks.Code = uploaded_sheet[i][5]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    db.Stocks.Add(_stocks);
                    db.SaveChanges();
                    countRecs++;
                    Console.WriteLine("Data Saved..." + countRecs);
                }
                #endregion


            }
            catch (Exception e)
            {

                throw;
            }
        }

        public void SaveStockTypesFile()
        {
            Worksheet uploaded_sheet;
            int countRecs = 0;
            try
            {
                string file_Name = "C:\\Test\\my_StockTypes.xml";
                Stream stockfile = File.OpenRead(file_Name);
                var stockWorkBook = ExcelXmlWorkbook.Import(stockfile);

                uploaded_sheet = stockWorkBook.GetSheetByName("StockTypes");
                var stockID = uploaded_sheet[0, 1].Value;

                StockType _stocks = new StockType();

                #region For Statement
                //Our For statement to store data
                for (int i = 1; i < uploaded_sheet.RowCount; i++)
                {

                    bool hasvalue = false;
                    //checks if the worksheet contains any values
                    for (int u = 0; u < uploaded_sheet.ColumnCount; u++)
                    {
                        if (uploaded_sheet[i][u].Value != null)
                        {
                            hasvalue = true;
                            break;
                        }
                    }
                    if (!hasvalue) continue;

                    //check to see if there is any data 
                    if (uploaded_sheet[i][0].Value != null)
                    {
                        try
                        {
                            
                            string test = uploaded_sheet[i][0]?.Value.ToString();
                            _stocks.StockTypeID = test;
                            
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    if (uploaded_sheet[i][1].Value != null)
                    {
                        try
                        {
                            _stocks.StockExchangeID = uploaded_sheet[i][1]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][2].Value != null)
                    {
                        try
                        {
                            _stocks.Name = uploaded_sheet[i][2]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }


                    if (uploaded_sheet[i][3].Value != null)
                    {
                        try
                        {
                            _stocks.Code = uploaded_sheet[i][3]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][4].Value != null)
                    {
                        try
                        {
                            string _active = uploaded_sheet[i][4]?.Value.ToString();
                            if (int.Parse(_active)==1)
                            {
                                _stocks.Active = true;
                            }
                            else if (int.Parse(_active) == 0)
                            {
                                _stocks.Active = false;
                            }
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    db.StockTypes.Add(_stocks);
                    db.SaveChanges();
                    countRecs++;
                    Console.WriteLine("Data Saved..." + countRecs);
                }
                #endregion


            }
            catch (Exception e)
            {

                throw;
            }
        }

        public void SaveStockTypeIds()
        {
            int count = 0;
            foreach (var item in db.Stocks.ToList())
            {
                var stock_stockTypeId = item.Stock_TypeID;

                foreach (var stockTest in db.StockTypes.ToList())
                {
                    var stockTypeId = stockTest.StockTypeID;
                    int stockTypeID = stockTest.stock_TypeID;

                    if (string.Equals(stock_stockTypeId, stockTypeId))
                    {
                        item.stock_type_ID= stockTypeID;
                        db.SaveChanges();
                        count++;
                    }
                }

            }
            Console.WriteLine("{0} rows updated",count);
        }

        public void SaveStockExchangeIds()
        {
            int count = 0;
            foreach (var item in db.Stocks.ToList())
            {
                var stock_stockTypeId = item.Stock_ExchangeID;

                foreach (var stockTest in db.StockExchanges.ToList())
                {
                    var stockExchangeId = stockTest.StockEchangeID;
                    int _stockExchangeID = stockTest.stock_exchangeID;

                    if (string.Equals(stock_stockTypeId, stockExchangeId))
                    {
                        item.stockExchange_ID = _stockExchangeID;
                        db.SaveChanges();
                        count++;
                    }
                }

            }
            Console.WriteLine("{0} rows updated", count);
        }

        public void SaveStockExchangeID()
        {
            int count = 0;
            foreach (var item in db.StockTypes.ToList())
            {
                //get the stockTypeExchangeID
                var stockTypeExchangeID = item.StockExchangeID;
                //compare the id
                foreach (var test in db.StockExchanges.ToList())
                {
                    //get the stock exchange id
                    var stockExchangeID = test.StockEchangeID;
                    var _stockExchangeID = test.stock_exchangeID; 

                    if (string.Equals(stockTypeExchangeID, stockExchangeID))
                    {
                        item.stockExchange_id = _stockExchangeID;
                        db.SaveChanges();
                        count++;
                    }
                }
                Console.WriteLine("{0} rows updated",count);
            }
        }
        public void SaveStockExchangesFile()
        {
            Worksheet uploaded_sheet;
            int countRecs = 0;
            try
            {
                string file_Name = "C:\\Test\\myStockExchanges.xml";
                Stream stockfile = File.OpenRead(file_Name);
                var stockWorkBook = ExcelXmlWorkbook.Import(stockfile);

                uploaded_sheet = stockWorkBook.GetSheetByName("StockExchanges");
                var stockID = uploaded_sheet[0, 1].Value;

                StockExchanx _stocks = new StockExchanx();

                #region For Statement
                //Our For statement to store data
                for (int i = 1; i < uploaded_sheet.RowCount; i++)
                {

                    bool hasvalue = false;
                    //checks if the worksheet contains any values
                    for (int u = 0; u < uploaded_sheet.ColumnCount; u++)
                    {
                        if (uploaded_sheet[i][u].Value != null)
                        {
                            hasvalue = true;
                            break;
                        }
                    }
                    if (!hasvalue) continue;

                    //check to see if there is any data 
                    if (uploaded_sheet[i][0].Value != null)
                    {
                        try
                        {

                            string test = uploaded_sheet[i][0]?.Value.ToString();
                            _stocks.StockEchangeID = test;

                        }
                        catch (Exception e)
                        {
                        }
                    }
                    if (uploaded_sheet[i][1].Value != null)
                    {
                        try
                        {
                            _stocks.Name = uploaded_sheet[i][1]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }

                    if (uploaded_sheet[i][2].Value != null)
                    {
                        try
                        {
                            _stocks.Code = uploaded_sheet[i][2]?.Value.ToString();
                        }
                        catch (Exception e)
                        {
                        }
                    }
                    db.StockExchanges.Add(_stocks);
                    db.SaveChanges();
                    countRecs++;
                    Console.WriteLine("Data Saved..." + countRecs);
                }
                #endregion


            }
            catch (Exception e)
            {

                throw;
            }
        }
        static void Main(string[] args)
        {
            Program pg = new Program();
            Console.WriteLine("Innova Starting...");
            pg.SaveStockExchangeIds();
            //pg.SaveStockTypeIds();
            //pg.SaveStockExchangeID();
            // pg.SaveStockExchangesFile();
            //pg.SaveStockTypesFile();
            //pg.SaveStockFile();
            Console.WriteLine("Program Finished..");
            Console.ReadLine();
        }
    }
}
