﻿//Mediates with our web api 

var app = angular.module('innova', []);
//subject to Change
var url = "http://localhost:59843/api/DownloadStockFile";
var searchUrl = "http://localhost:59843/api/SearchStock";

app.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function () {
                scope.$apply(function () {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

app.service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function (file, uploadUrl) {
        var fd = new FormData();
        fd.append('file', file);

        $http({
            method: "POST",
            transformRequest: angular.identity,
            headers: { 'Content-Type': undefined },
            url: url + '?name=' + fd
        }).then(function OnSucces(response) {
            $scope.test = response.data;
        }, function OnError(response) {
            $scope.test = response.statusText;
        });

        //$http.post(uploadUrl, fd, {
        //    transformRequest: angular.identity,
        //    headers: { 'Content-Type': undefined }
        //})

        //.success(function () {
        //})

        //.error(function () {
        //});
    }
}]);
//our controller
app.controller('upload', ['$scope', '$http', 'fileUpload', function ($scope, $http, fileUpload) {

    $scope.uploadFile = function () {
        var stockPriceFile = $scope.myFile;
        debugger;
        $http({
            method: "POST",
            contentType: 'multipart/form-data',
            url: url + '?name=' + stockPriceFile
        }).then(function OnSucces(response) {
            $scope.test = response.data;
        }, function OnError(response) {
            $scope.test = response.statusText;
        });
    };

    $scope.DownloadStockFile = function () {
        var test = $scope.SelectedStock;
    }

    $scope.searchStock = function () {
        var searchedStock = $("#searchStock").val();
        debugger;
        $http({
            method: "POST",
            url: searchUrl + "?search=" + searchedStock,
        }).then(function OnSucces(response) {
            $scope.stockPriceData = response.data;
            debugger;
        }, function OnError(response) {
            $scope.stockError = response.statusText;
            debugger;
        });
    };

}]);

app.controller('updateTable', ['$scope', '$http', function ($scope, $http) {
      $scope.stockPriceData = null;

        $http({
            method: "GET",
            url: url
        }).then(function OnSucces(response) {
            $scope.stockPriceData = response.data;
            debugger;
        }, function OnError(response) {
            $scope.stockError = response.statusText;
            debugger;
        });

        $scope.searchStock = function () {
            var searchedStock = $("#searchStock").val();
            debugger;
            $http({
                method: "POST",
                url: searchUrl + "?search=" + searchedStock,
            }).then(function OnSucces(response) {
                $scope.stockPriceData = response.data;
                debugger;
            }, function OnError(response) {
                $scope.stockError = response.statusText;
                debugger;
            });
        };
      
    
}]);