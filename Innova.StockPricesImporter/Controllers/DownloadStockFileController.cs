﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;
using System.Threading.Tasks;
using BusinLogicLayer.Services;
using BusinLogicLayer.BusinessModels;

namespace Innova.StockPricesImporter.Controllers
{
    /// <summary>
    /// Manages Upload Requests
    /// </summary>
    public class DownloadStockFileController : ApiController
    {
        #region Properties
        StockServices sServices = new StockServices();
        #endregion

        #region Methods
        //Gets the stock data on load
        [HttpGet]
        public List<StockViewModel> GetStocks()
        {
            return sServices.GetStocks();
        }

        [HttpPost]
        public void Download(string name)
        {
            //for demonstration purposes.The method below processes Excel xml
            //files from the directory
            sServices.SaveStockFile();
            try
            {
                if (!string.IsNullOrEmpty(name))
                {
                    //string filename = name;
                    //int fnameLength = filename.Length;
                    //int pos = filename.IndexOf("P");
                    //var fileLength = fnameLength - pos;
                    //var stockFile = filename.Substring(pos, fileLength);
                    //string filePath = HttpContext.Current.Server.MapPath("~/UploadedStocks/") + stockFile;
                    //Stream uploadFileStream = File.OpenRead(name);
                    //stockOperations.ProcessStockFil();
                }
            }
            catch (Exception e)
            {

                //throw;
            }

        }
 
        #endregion
    }
}
