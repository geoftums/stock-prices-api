﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinLogicLayer.Services;

namespace Innova.StockPricesImporter.Controllers
{
    /// <summary>
    /// controller to perform searching of stocks
    /// </summary>
    public class SearchStockController : ApiController
    {
        #region  Properties
        SearchStockServices sv = new SearchStockServices();
        #endregion

        #region Methods
        [HttpPost]
        public IHttpActionResult SearchStock(string search)
        {
            if (!String.IsNullOrEmpty(search))
            {
               var result= sv.SearchStock(search);
                return Ok(result);
            }
            return NotFound();
        }

        #endregion
    }
}
