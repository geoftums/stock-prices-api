﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innova.StockPricesImporter.Data.Model
{
    public class StockExchanges
    {
        public Guid StockEchangeID { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public ICollection<StockTypes> StockTypes { get; set; }
        public ICollection<Stocks> Stocks { get; set; }
    }
}
